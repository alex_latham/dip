import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt

#data = 'DATA\\nZnO_sNR on n-GaN #556 FIB\\2017_08_04__15.46_NMIV_p13iv1n.txt'
d556 = [
    'DATA\\nZnO_sNR on n-GaN #556 FIB\\2017_08_04__15.46_NMIV_p13iv1n.txt',
    'DATA\\nZnO_sNR on n-GaN #556 FIB\\2017_08_04__15.47_NMIV_p13iv1p.txt',
]

fig = plt.figure(num=None, figsize=(6, 4), dpi=200, facecolor='w', edgecolor='k')
#fig = plt.figure()
plt.title('n-ZnO NR / n-GaN')
# --- d556
d = np.loadtxt(d556[0])
half = int(len(d[:,0])/2)
x,y = -d[:,0][:half], -d[:,1][:half]
#plt.loglog(x, y, 'o', color="#ffa500", label='without FIB')

plt.loglog(x, y, 'v', color="#ffa500")

def fit(x, y):
    logx = np.log(x)
    logy = np.log(y)
    coeffs = np.polyfit(logx, logy, deg=1)
    poly = np.poly1d(coeffs)
    return poly
x_pow = x[1:5]
y_pow = y[1:5]
poly1 = fit(x_pow, y_pow)
print(poly1)  # power coefficient
x_pow_cont = np.arange(x_pow[0], x_pow[-1], 0.001)
yfit1 = lambda x: np.exp(poly1(np.log(x_pow_cont)))
plt.loglog(x_pow_cont, yfit1(x_pow_cont), color="#1e76b3")
plt.annotate('$I\\approx V$ or $I\\approx V^n$', xy=(8.7, 6e-9), xytext=(1.4, 3e-12), color='#1e76b3')

# --- middle section?
# x_pow, y_pow = x[5:8], y[5:8]
# poly1 = fit(x_pow, y_pow)
# print(poly1)  # power coefficient
# x_pow_cont = np.arange(x_pow[0], x_pow[-1], 0.001)
# yfit1 = lambda x: np.exp(poly1(np.log(x_pow_cont)))
# plt.loglog(x_pow_cont, yfit1(x_pow_cont), color="#1e76b3")
# #plt.annotate('$I\\approx V^n$', xy=(8.7, 6e-9), xytext=(8.7, 6e-9), color='#1e76b3')

x_pow, y_pow = x[8:], y[8:]
poly1 = fit(x_pow, y_pow)
print(poly1)  # power coefficient
x_pow_cont = np.arange(x_pow[0], x_pow[-1], 0.001)
yfit1 = lambda x: np.exp(poly1(np.log(x_pow_cont)))
plt.loglog(x_pow_cont, yfit1(x_pow_cont), color="#1e76b3")
plt.annotate('$I\\approx V^2$', xy=(8.7, 6e-9), xytext=(14.2, 4e-8), color='#1e76b3')

plt.annotate('e$^{-}$ trap filling', xy=(8.7, 6e-9), xytext=(5.9, 1.8e-9), color='#1e76b3')

plt.xlabel('Voltage (V)')
plt.ylabel('Current (A)')

plt.tight_layout()
plt.savefig('C:\Syncs\Main\Responsible\phd\epfl_roke\pre2\plt\\nn01.png')

plt.show()

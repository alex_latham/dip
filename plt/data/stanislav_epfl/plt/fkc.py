import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt

#data = 'DATA\\nZnO_sNR on n-GaN #556 FIB\\2017_08_04__15.46_NMIV_p13iv1n.txt'
d692 = [
    'DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__13.52_NMIV_692_12#3_iv2p.txt',
    'DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__13.55_NMIV_692_12#3_iv2n.txt',
    'DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__14.40_NMIV_692_12#4_iv1p.txt',
    'DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__14.43_NMIV_692_12#4_iv1n.txt',
    'DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__14.56_NMIV_692_12#4_iv3p.txt',
    'DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__14.58_NMIV_692_12#4_iv3n.txt',
]
d527 = [
    'DATA\\nZnO_sNR on pGaN #527 no-FIB\\2017_11_29__18.56_NMIV_snr527-1-iv2n.txt',
    'DATA\\nZnO_sNR on pGaN #527 no-FIB\\2017_11_29__18.57_NMIV_snr527-1-iv2p.txt',
    'DATA\\nZnO_sNR on pGaN #527 no-FIB\\2017_11_29__19.13_NMIV_snr527-1-iv3p.txt',
    'DATA\\nZnO_sNR on pGaN #527 no-FIB\\2017_11_29__19.14_NMIV_snr527-1-iv3-2n.txt',
    'DATA\\nZnO_sNR on pGaN #527 no-FIB\\2017_11_29__19.36_NMIV_snr527-iv4n.txt',
    'DATA\\nZnO_sNR on pGaN #527 no-FIB\\2017_11_29__19.37_NMIV_snr527-iv4p.txt',
]

#fig = plt.figure(num=None, figsize=(6, 4.5), dpi=200, facecolor='w', edgecolor='k')
fig = plt.figure()
# --- d527
d = np.loadtxt(d527[3])
half = int(len(d[:,0])/2)
x,y = -d[:,0][:half], -d[:,1][:half]
#plt.loglog(x, y, 'o', color="#ffa500", label='without FIB')

lx, ly = np.log(x[1:10]), np.log(y[1:10])
plt.loglog(x, y, 'o', color="#ffa500", label='without FIB')

def ex(x, a, b, c):
    return a*np.exp(b*x)+c
def expfit(x, y):
    lx, ly = np.log(x), np.log(y)
    popt, pcov = curve_fit(ex, lx, ly)
    fit_vals = ex(lx, *popt)
    return lx, fit_vals
lx, fit_vals = expfit(x[1:10], y[1:10])
plt.loglog(np.exp(lx), np.exp(fit_vals))

plt.show()

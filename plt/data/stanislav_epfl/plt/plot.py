import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt

#data = 'DATA\\nZnO_sNR on n-GaN #556 FIB\\2017_08_04__15.46_NMIV_p13iv1n.txt'
d692 = [
    'DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__13.52_NMIV_692_12#3_iv2p.txt',
    'DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__13.55_NMIV_692_12#3_iv2n.txt',
    'DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__14.40_NMIV_692_12#4_iv1p.txt',
    'DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__14.43_NMIV_692_12#4_iv1n.txt',
    'DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__14.56_NMIV_692_12#4_iv3p.txt',
    'DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__14.58_NMIV_692_12#4_iv3n.txt',
]
d527 = [
    'DATA\\nZnO_sNR on pGaN #527 no-FIB\\2017_11_29__18.56_NMIV_snr527-1-iv2n.txt',
    'DATA\\nZnO_sNR on pGaN #527 no-FIB\\2017_11_29__18.57_NMIV_snr527-1-iv2p.txt',
    'DATA\\nZnO_sNR on pGaN #527 no-FIB\\2017_11_29__19.13_NMIV_snr527-1-iv3p.txt',
    'DATA\\nZnO_sNR on pGaN #527 no-FIB\\2017_11_29__19.14_NMIV_snr527-1-iv3-2n.txt',
    'DATA\\nZnO_sNR on pGaN #527 no-FIB\\2017_11_29__19.36_NMIV_snr527-iv4n.txt',
    'DATA\\nZnO_sNR on pGaN #527 no-FIB\\2017_11_29__19.37_NMIV_snr527-iv4p.txt',
]

fig = plt.figure(num=None, figsize=(6, 4), dpi=200, facecolor='w', edgecolor='k')
#fig = plt.figure()

# --- d692
d = np.loadtxt(d692[3])
half = int(len(d[:,0])/2)
x, y = -d[:,0][:half], -d[:,1][:half]
plt.loglog(x, y, 'v', color="#ffa500", label="with FIB")

# --- 6 to 18 is exp()
def ex(x, alpha, c):
    return np.exp(alpha*x)*c
x_exp = x[6:18]
y_exp = y[6:18]
popt, pcov = curve_fit(ex, x_exp, y_exp)
#print(popt)  # exp coefficient
x_exp_cont = np.arange(x_exp[0], x_exp[-1], 0.001)
plt.loglog(x_exp_cont, ex(x_exp_cont, popt[0], popt[1]), color="#1e76b3")
plt.annotate('$I\\approx e^{\\alpha V}$', xy=(4, 6e-11), xytext=(4, 6e-11), color='#1e76b3')

# # --- 19 to end is V^n
def fit(x, y):
    logx = np.log(x)
    logy = np.log(y)
    coeffs = np.polyfit(logx, logy, deg=1)
    poly = np.poly1d(coeffs)
    return poly
x_pow = x[19:]
y_pow = y[19:]
poly1 = fit(x_pow, y_pow)
#print(poly1)  # power coefficient
x_pow_cont = np.arange(x_pow[0], x_pow[-1], 0.001)
yfit1 = lambda x: np.exp(poly1(np.log(x_pow_cont)))
plt.loglog(x_pow_cont, yfit1(x_pow_cont), color="#1e76b3")
plt.annotate('$I\\approx V^n$', xy=(8.7, 6e-9), xytext=(8.7, 6e-9), color='#1e76b3')

# --- cosmetics
plt.title('n-ZnO NR / p-GaN')
plt.xlabel('Voltage (V)')
plt.ylabel('Current (A)')
plt.legend(loc='upper left')
plt.xlim([0.2, 25])
plt.ylim([5e-13, 1e-7])

plt.tight_layout()
plt.savefig('C:\Syncs\Main\Responsible\phd\epfl_roke\pre2\plt\pn01.png')

# --- 3e-12 noise + annotation
#plt.axhline(y=3e-12, color='r', linestyle='--')
#plt.annotate('noise level', xy=(7, 2e-12), xytext=(10, 1.7e-12), color='red')

plt.tight_layout()
plt.savefig('C:\Syncs\Main\Responsible\phd\epfl_roke\pre2\plt\pn02.png')



# --- d527
d = np.loadtxt(d527[3])
half = int(len(d[:,0])/2)
x,y = -d[:,0][:half], -d[:,1][:half]
plt.loglog(x, y, 'o', color="#ffa500", label='without FIB')

# --- 0 to 10 is exp()
def ex(x, a, b, c):
    return a*np.exp(b*x)+c
def expfit(x, y):
    lx, ly = np.log(x), np.log(y)
    popt, pcov = curve_fit(ex, lx, ly)
    lx_dense = np.arange(lx[0], lx[-1], 0.001)
    fit_vals = ex(lx_dense, *popt)
    return lx_dense, fit_vals
lx, fit_vals = expfit(x[2:9], y[2:9])
plt.loglog(np.exp(lx), np.exp(fit_vals))
# plt.loglog(x_exp_cont, ex(x_exp_cont, popt[0], popt[1]), color="#1e76b3")
plt.annotate('$I\\approx e^{\\alpha V}$', xy=(4, 6e-11), xytext=(0.7, 1.5e-10), color='#1e76b3')

# --- 10 to end is V^n
def fit(x, y):
    logx = np.log(x)
    logy = np.log(y)
    coeffs = np.polyfit(logx, logy, deg=1)
    poly = np.poly1d(coeffs)
    return poly
x_pow = x[10:]
y_pow = y[10:]
poly1 = fit(x_pow, y_pow)
print(poly1)  # power coefficient
x_pow_cont = np.arange(x_pow[0], x_pow[-1], 0.001)
yfit1 = lambda x: np.exp(poly1(np.log(x_pow_cont)))
plt.loglog(x_pow_cont, yfit1(x_pow_cont), color="#1e76b3")
plt.annotate('$I\\approx V^2$', xy=(2.5, 8e-8), xytext=(2.1, 5e-8), color='#1e76b3')


plt.legend(loc='upper left')


# ---
plt.tight_layout()
plt.savefig('C:\Syncs\Main\Responsible\phd\epfl_roke\pre2\plt\pn03.png')

plt.show()

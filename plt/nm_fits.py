import numpy as np
from scipy.optimize import curve_fit
import matplotlib as mpl
mpl.use("pgf")
pgf_with_rc_fonts = {
    "font.family": "serif",
    "font.serif": [],                   # use latex default serif font
    "font.sans-serif": ["DejaVu Sans"], # use a specific sans-serif font
}
mpl.rcParams.update(pgf_with_rc_fonts)
import matplotlib.pyplot as plt
import settings as s

def half_flip_unpack(d, pos=0):
    half = int(len(d[:,0])/2)
    if pos==0:
        x, y = -d[:,0][:half], -d[:,1][:half]
    if pos==1:
        x, y = -d[:,0][half:], -d[:,1][half:]
    return x, y

def exp(x, alpha, c):
    return np.exp(alpha*x)*c

def exp_fit(x, y):
    ''' Fits (x,y) data with y = C*e^(alpha*x)
        returns poly => poly[0] is C, poly[1] is alpha
    '''
    if abs:
        x = np.abs(x)
        logy = np.log(np.abs(y))
    else:
        x = x
        logy = np.log(y)
    coeffs = np.polyfit(x, logy, deg=1)
    poly = np.poly1d(coeffs)
    return poly

def pow_fit(x, y):
    logx = np.log10(x)
    logy = np.log10(y)
    coeffs = np.polyfit(logx, logy, deg=1)
    poly = np.poly1d(coeffs)
    return poly

def aC_to_nIs(a, C):
    kB = 1.380648e-23  # [J.K-1]
    T = 273.15+22  # [K]
    q = 1.602176e-19 # [C]
    Is = np.exp(C)
    n = q/(a*kB*T)
    label = "$n$={:.4}\n$I_s$={:.4}".format(n, Is)
    return label

d527 = 'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2018_01_22__16.24_NMIV_527_nr3_neg.txt'
d692 = 'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__14.43_NMIV_692_12#4_iv1n.txt'

fig = plt.figure(num=None, figsize=s.figsize, dpi=s.dpi, facecolor='w', edgecolor='k')

lang = "cs"
if lang=="cs":
    label_fib = s.label_fib_cs
    label_nofib  = s.label_nofib_cs
else:
    label_fib = s.label_fib
    label_nofib  = s.label_nofib

# Data sample #692 (FIB patterned)
d = np.loadtxt(d692)
x, y = half_flip_unpack(d)
plt.loglog(x, y, 'v', label=label_fib, color=s.data2)

# Fitting 6 to 18 linearly in x/ln(y) (lower part of FIB patterned)
x_fit = x[6:18]
y_fit = y[6:18]
p = exp_fit(x_fit, y_fit)
print(p)  # exp coefficients
x_fit_dense = np.arange(x_fit[0], x_fit[-1], 0.001)
y_fit = lambda x: np.exp(p(x_fit_dense))
plt.loglog(x_fit_dense, y_fit(x_fit_dense), color=s.fit2)
plt.annotate('$I\\sim e^{\\alpha U}$', xy=(4, 6e-11), xytext=(4, 6e-11), color=s.labelcol)
print(aC_to_nIs(*p))

# Fitting 19 to end linearly in log(x)/log(y) (top part of FIB patterned)
x_fit = x[19:]
y_fit = y[19:]
p = pow_fit(x_fit, y_fit)
print(p)  # power coefficient
x_fit_dense = np.arange(x_fit[0], x_fit[-1], 0.001)
y_fit = lambda x: np.power(10, p(np.log10(x_fit_dense)))
plt.loglog(x_fit_dense, y_fit(x_fit_dense), color=s.fit2)
plt.annotate('$I\\sim U^n$', xy=(8.7, 6e-9), xytext=(8.7, 6e-9), color=s.labelcol)


# Data sample #527
d = np.loadtxt(d527)
x, y = half_flip_unpack(d)
plt.loglog(x, y, 'o', label=label_nofib, color=s.data1)

# Fitting 2 to 10 linearly in x/ln(y) (bottom part of non-patterned)
x_fit = x[2:10]
y_fit = y[2:10]
p = exp_fit(x_fit, y_fit)
print(p)  # exp coefficients
x_fit_dense = np.arange(x_fit[0], x_fit[-1], 0.001)
y_fit = lambda x: np.exp(p(x_fit_dense))
plt.loglog(x_fit_dense, y_fit(x_fit_dense), color=s.fit1)
plt.annotate('$I\\sim e^{\\alpha U}$', xy=(4, 6e-11), xytext=(0.5, 2e-11), color=s.labelcol)
print(aC_to_nIs(*p))

# Fitting 10 to 26 linearly in log(x)/log(y) (middle part of non-patterned)
x_fit = x[10:26]
y_fit = y[10:26]
p = pow_fit(x_fit, y_fit)
print(p)  # power coefficient
x_fit_dense = np.arange(x_fit[0], x_fit[-1], 0.001)
y_fit = lambda x: np.power(10, p(np.log10(x_fit_dense)))
plt.loglog(x_fit_dense, y_fit(x_fit_dense), color=s.fit1)
plt.annotate('$I\\sim U^n$', xy=(2.5, 8e-8), xytext=(1.7, 5e-8), color=s.labelcol)

# Fitting 30 to end linearly in log(x)/log(y) (middle part of non-patterned)
x_fit = x[30:]
y_fit = y[30:]
p = pow_fit(x_fit, y_fit)
print(p)  # power coefficient
x_fit_dense = np.arange(x_fit[0], x_fit[-1], 0.001)
y_fit = lambda x: np.power(10, p(np.log10(x_fit_dense)))
plt.loglog(x_fit_dense, y_fit(x_fit_dense), color=s.fit1)
plt.annotate('$I\\sim U^2$', xy=(2.5, 8e-8), xytext=(5.5, 1.2e-6), color=s.labelcol)


# Předěly
ax = plt.gca()
ax.annotate("", xy=(1.8, 3.9e-10), xytext=(1.35, 1.02e-9), arrowprops=dict(arrowstyle="-", linestyle="--", color=s.labelcol2))
ax.annotate("", xy=(5.05, 4.5e-8), xytext=(4.2, 2.3e-7), arrowprops=dict(arrowstyle="-", linestyle="--", color=s.labelcol2))
ax.annotate("", xy=(10.1, 1.1e-10), xytext=(7.8, 4.3e-10), arrowprops=dict(arrowstyle="-", linestyle="--", color=s.labelcol2))

plt.ylim([1e-13, 5e-6])
plt.legend(loc='upper left')

plt.ylabel(s.label('I'))
plt.xlabel(s.label('U'))

plt.tight_layout()

plt.savefig('out/nm_fits.pdf')
plt.savefig('out/nm_fits.pgf')
s.fix_pgf_lyx('out/nm_fits.pgf')

plt.xlabel(s.label_cs('U'))
plt.ylabel(s.label_cs('I'))
plt.savefig('C:\Syncs\Main\Skola\FJFI_2018_LS\sdp\plt\\nm_fits' + '.pdf', dpi=300)
#plt.show()

import numpy as np
from scipy.optimize import curve_fit
import matplotlib as mpl
import settings as s
mpl.use("pgf")
pgf_with_rc_fonts = {
    "font.family": "serif",
    "font.serif": [],                   # use latex default serif font
    "font.sans-serif": ["DejaVu Sans"], # use a specific sans-serif font
}
mpl.rcParams.update(pgf_with_rc_fonts)
import matplotlib.pyplot as plt

def half_o(d, first=True):
    half = int(len(d[:,0])/2)
    if first:
        x, y = -d[:,0][:half], -d[:,1][:half]
    else:
        x, y = -d[:,0][half:], -d[:,1][half:]
    return x, y*1e9

def half(d, first=True):
    half = int(len(d[:,0])/2)
    if first:
        x, y = -d[:,0][:half], -d[:,1][:half]
        x = x[(y<68e-9) & (y>-28e-9)]
        y = y[(y<68e-9) & (y>-28e-9)]
    else:
        x, y = -d[:,0][half:], -d[:,1][half:]
        x = x[(y<68e-9) & (y>-28e-9)]
        y = y[(y<68e-9) & (y>-28e-9)]
    return x, y*1e9


#data = 'DATA\\nZnO_sNR on n-GaN #556 FIB\\2017_08_04__15.46_NMIV_p13iv1n.txt'
d556 = [
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on n-GaN #556 FIB\\2017_08_04__15.46_NMIV_p13iv1n.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on n-GaN #556 FIB\\2017_08_04__15.47_NMIV_p13iv1p.txt',
]
d692 = [
    # 'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__14.40_NMIV_692_12#4_iv1p.txt', # red one overview
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__14.56_NMIV_692_12#4_iv3p.txt', # red one overview (pos)
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__14.43_NMIV_692_12#4_iv1n.txt', # red one overview (neg)
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #692 FIB\\2018_02_05__11.44_NMIV_test_sweep.txt', # black one overview
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #692 FIB\\2018_02_05__11.47_NMIV_test_sweep.txt', # black one overview
    #'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__13.52_NMIV_692_12#3_iv2p.txt',
    #'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__13.55_NMIV_692_12#3_iv2n.txt',
    #'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__14.58_NMIV_692_12#4_iv3n.txt',
]
d527 = [
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2017_11_29__18.56_NMIV_snr527-1-iv2n.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2017_11_29__18.57_NMIV_snr527-1-iv2p.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2017_11_29__19.13_NMIV_snr527-1-iv3p.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2017_11_29__19.14_NMIV_snr527-1-iv3-2n.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2017_11_29__19.36_NMIV_snr527-iv4n.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2017_11_29__19.37_NMIV_snr527-iv4p.txt',
]

fig, ax1 = plt.subplots(num=None, figsize=s.figsize, dpi=s.dpi, facecolor='w', edgecolor='k')
#fig = plt.figure()

ls1 = "v"
ls2 = "o"
ms = 5

lang = "cs"
if lang=="cs":
    label_fib = s.label_fib_cs
    label_nofib  = s.label_nofib_cs
else:
    label_fib = s.label_fib
    label_nofib  = s.label_nofib
# --- d692
d = np.loadtxt(d692[0])
x, y = half(d, first=False)
plt.plot(x, y, ls1, color=s.red, label=label_fib, markersize=ms)

d = np.loadtxt(d692[1])
x, y = half(d, first=False)
plt.plot(x, y, ls1, color=s.red, markersize=ms)

# --- noFIB?
d = np.loadtxt(d692[2])
x, y = half(d)
plt.plot(x, y, ls2, color=s.blue, label=label_nofib, markersize=ms)

d = np.loadtxt(d692[3])
x, y = half(d, first=True)
plt.plot(x, y, ls2, color=s.blue, markersize=ms)


#plt.xlim([-30,22])
plt.ylim([-30, 70])


if lang == "cs":
    plt.xlabel(s.label_cs('U'))
    plt.ylabel(s.label_cs('I', prefix='n'))
else:
    plt.xlabel(s.label('U'))
    plt.ylabel(s.label('I', prefix='n'))

plt.legend(loc='lower right')


plt.tight_layout()



from mpl_toolkits.axes_grid.inset_locator import (inset_axes, InsetPosition, mark_inset)
# Create a set of inset Axes: these should fill the bounding box allocated to
# them.
ax2 = plt.axes([0,0,1,1])
# Manually set the position and relative size of the inset axes within ax1
ip = InsetPosition(ax1, [0.13,0.50,0.4,0.45])
ax2.set_axes_locator(ip)
# Mark the region corresponding to the inset axes on ax1 and draw lines
# in grey linking the two axes.
#mark_inset(ax1, ax2, loc1=2, loc2=4, fc="none", ec='0.5')

dn = np.loadtxt('data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #692 FIB\\iv1n.txt', skiprows=3)

ax2.plot(dn[:,0], dn[:,1]*1e6, '-', color='black')
ax2.set_ylabel(s.label('I', prefix='µ'))
ax2.set_xlabel(s.label('U'))
ax2.set_xlim([0,1.1])
ax2.set_ylim([0,170])

# ---
plt.savefig('out/nm_overview.pdf')
plt.savefig('out/nm_overview.pgf')

s.fix_pgf_lyx('out/nm_overview.pgf')

ax2.set_ylabel(s.label_cs('I', prefix='µ'))
ax2.set_xlabel(s.label_cs('U'))
plt.savefig('C:\Syncs\Main\Skola\FJFI_2018_LS\sdp\plt\\nm_overview' + '.pdf', dpi=300)

#plt.show()

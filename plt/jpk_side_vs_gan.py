import numpy as np
import matplotlib as mpl
import settings as s
mpl.use("pgf")
pgf_with_rc_fonts = {
    "font.family": "serif",
    "font.serif": [],                   # use latex default serif font
    "font.sans-serif": ["DejaVu Sans"], # use a specific sans-serif font
}
mpl.rcParams.update(pgf_with_rc_fonts)
import matplotlib.pyplot as plt

from os import listdir
from os.path import isfile, join, split
import sys

def get_jpk_index(filename):
    with open(filename, 'r') as myfile:
        index_str = myfile.read().split('\n')[8].split(' ')[2]
        index = int(index_str)
    return index

def half(data, pos=0):
    h = int(len(data)/2)
    if pos:
        return data[:h]
    else:
        return data[h:]

def flip(d):
    return -d

def strip_compliance(voltage, current):
    #print(data.shape)
    index = np.where(np.abs(current)<100e-9)
    i = current[index]
    v = voltage[index]
    return i, v

def get_files_from_folder(folder):
    full_filelist = [join(folder, f).replace('\\', '/') for f in listdir(folder) if (isfile(join(folder, f)) and (f[-4:]=='.txt'))]
    return full_filelist

def pol(v, c, neg=False):
    if neg:
        out_v = -v[v<0]
        out_c = -c[v<0]
    else:
        out_v = v[v>0]
        out_c = c[v>0]
    return out_v, out_c

folder = 'data/jpk_our/'
filelist = get_files_from_folder(folder)


colorlist = [s.red, s.blue]

# -------- LINEAR
fig = plt.figure(num=1, figsize=s.figsize, dpi=s.dpi, facecolor='w', edgecolor='k')
plt.clf()

m = len(filelist)
for i in range(0,m):
    data = np.loadtxt(filelist[i]).T
    I, U = strip_compliance(flip(half(data[4], pos=0)), flip(half(data[0], pos=0)))
    lang="cs"
    if lang=="cs":
        positions = {1:'GaN substrát (pozice 2)', 2:'ZnO nanotyčka (pozice 1)'}
    else:
        positions = {1:'GaN substrate (position 3)', 2:'ZnO nanorod (position 2)'}
    jpk_index = get_jpk_index(filelist[i])
    plt.plot(U, I*1e9, color = colorlist[int((i/m)*len(colorlist))], label = '{}'.format(positions[jpk_index]))
plt.xlabel(s.label('U'))
plt.ylabel(s.label('I', prefix='n'))
plt.legend(loc='upper left')
#plt.title('IV')
plt.xlim([-5.1,5.1])
plt.ylim([-105,105])
plt.tight_layout()

plt.savefig('out/jpk_side_vs_gan' + '.pdf', dpi=300)
plt.savefig('out/jpk_side_vs_gan' + '.pgf', dpi=300)
s.fix_pgf_lyx('out/jpk_side_vs_gan' + '.pgf')

plt.xlabel(s.label_cs('U'))
plt.ylabel(s.label_cs('I', prefix='n'))
plt.savefig('C:\Syncs\Main\Skola\FJFI_2018_LS\sdp\plt\jpk_side_vs_gan' + '.pdf', dpi=300)

plt.xlim([-5.1,5.1])
plt.ylim([-10,10])
plt.tight_layout()

plt.savefig('out/jpk_side_vs_gan_zoom' + '.pdf', dpi=300)
plt.savefig('out/jpk_side_vs_gan_zoom' + '.pgf', dpi=300)
s.fix_pgf_lyx('out/jpk_side_vs_gan_zoom' + '.pgf')



#plt.savefig('side_vs_gan_lin.png')
#plt.show()

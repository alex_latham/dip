from colormap import *
clist = ["#1e76b3", "#ffa500", "#c33c54", "#ff8c42", ]
figsize = (6, 3.5)
dpi = 150

label_fib = 'FIB patterned'
label_nofib = 'non-patterned'

label_fib_cs = 'patternovaný FIB'
label_nofib_cs = 'nepatternovaný'

blue = '#0571B0'
darkblue = (5/256, 63/256, 126/256)
lightblue = (55/256, 163/256, 220/256)
purple = '#5E3B99'
altpurple1 = '#474B80'
altpurple2 = '#88264F'
red = '#CA001F'
#red = (202, 0, 31)
lightred = (252/256, 50/256, 81/256)
darkred = (152/256, 0/256, 31/256)

black = (0,0,0)

labelcol = (0.2,0.2,0.2)
labelcol2 = labelcol
data1, data2 = blue, red
fit1, fit2 = black, black

unknown = "#c33c54"
unknown2 = "#ff8c42"
unknown3 = "#1e76b3"

ufe_yellow = '#F6C122'
yellow = "#ffa500"
ufe_blue = "#1e76b3"

# labels according to https://journals.aps.org/authors/axis-labels-and-scales-on-graphs-h18
def label(quantity, prefix=""):
    if quantity=='U':
        return 'Voltage $U$\,({}V)'.format(prefix)
    if quantity=='I':
        return 'Current $I$\,({}A)'.format(prefix)
    if quantity=='lambda':
        return 'Wavelength $\lambda$\,({}nm)'.format(prefix)
    if quantity=='In':
        return 'Intensity $I$\,({}a.u.)'.format(prefix)
    else:
        raise ValueError('Quantity >{}< not implemented.'.format(quantity))

def label_cs(quantity, prefix=""):
    if quantity=='U':
        return 'Napětí $U$\,({}V)'.format(prefix)
    if quantity=='I':
        return 'Proud $I$\,({}A)'.format(prefix)
    if quantity=='lambda':
        return 'Vlnová délka $\lambda$\,({}nm)'.format(prefix)
    if quantity=='In':
        return 'Intenzita $I$\,({}a.u.)'.format(prefix)
    else:
        raise ValueError('Quantity >{}< not implemented.'.format(quantity))

def fix_pgf_lyx(addr):
    with open(addr, 'r', encoding='utf-8') as myfile:
        pgf_broken = myfile.read()
    with open(addr, 'w', encoding='utf-8') as f:
        f.write(pgf_broken.replace('−', '-'))

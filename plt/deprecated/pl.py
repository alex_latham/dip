import numpy as np

# -*- coding: utf-8 -*-

import matplotlib as mpl
mpl.use("pgf")
pgf_with_rc_fonts = {
    "font.family": "serif",
    "font.serif": [],                   # use latex default serif font
    "font.sans-serif": ["DejaVu Sans"], # use a specific sans-serif font
}
mpl.rcParams.update(pgf_with_rc_fonts)

import matplotlib.pyplot as plt

fig = plt.figure(num=None, figsize=(6, 3.5), dpi=100, facecolor='w', edgecolor='k')

names = ['tetrapods', 'needles', 'nanorods', 'shells', 'aligned nanorods', 'nanoribbons']
sets = [1,2,3,4,5,6]
for s in sets:
    data = np.loadtxt('data/pl/{}.csv'.format(s), delimiter=",")
    plt.plot(data[:,0], data[:,1], label=names[s-1])

plt.xlim([350,650])
plt.xlabel('Wavelength [nm]')
plt.ylabel('Intensity [a.u.]')
plt.legend(loc='best')

plt.tight_layout()

plt.savefig('out/pl.pdf')
plt.savefig('out/pl.pgf')

import numpy as np
from scipy.optimize import curve_fit
import matplotlib as mpl
mpl.use("pgf")
pgf_with_rc_fonts = {
    "font.family": "serif",
    "font.serif": [],                   # use latex default serif font
    "font.sans-serif": ["DejaVu Sans"], # use a specific sans-serif font
}
mpl.rcParams.update(pgf_with_rc_fonts)
import matplotlib.pyplot as plt
import settings as s

#data = 'DATA\\nZnO_sNR on n-GaN #556 FIB\\2017_08_04__15.46_NMIV_p13iv1n.txt'
d692 = [
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__13.52_NMIV_692_12#3_iv2p.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__13.55_NMIV_692_12#3_iv2n.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__14.40_NMIV_692_12#4_iv1p.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__14.43_NMIV_692_12#4_iv1n.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__14.56_NMIV_692_12#4_iv3p.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__14.58_NMIV_692_12#4_iv3n.txt',
]
d527 = [
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2017_11_29__18.56_NMIV_snr527-1-iv2n.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2017_11_29__18.57_NMIV_snr527-1-iv2p.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2017_11_29__19.13_NMIV_snr527-1-iv3p.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2017_11_29__19.14_NMIV_snr527-1-iv3-2n.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2017_11_29__19.36_NMIV_snr527-iv4n.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2017_11_29__19.37_NMIV_snr527-iv4p.txt',
]

dn = [
        'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2018_01_22__16.21_NMIV_527_nr3_neg.txt',
        'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2018_01_22__16.23_NMIV_527_nr3_pos.txt',
        'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2018_01_22__16.24_NMIV_527_nr3_neg.txt',
        'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2018_01_22__16.25_NMIV_527_nr3_pos.txt',
        'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2018_01_22__16.27_NMIV_527_nr3_neg.txt',
]

fig = plt.figure(num=None, figsize=s.figsize, dpi=s.dpi, facecolor='w', edgecolor='k')
#fig = plt.figure()

# --- d692
d = np.loadtxt(d692[3])
half = int(len(d[:,0])/2)
x, y = -d[:,0][:half], -d[:,1][:half]
plt.loglog(x, y, 'v', label=s.label_fib, color=s.data2)

# --- 6 to 18 is exp()
def ex(x, alpha, c):
    return np.exp(alpha*x)*c
x_exp = x[6:18]
y_exp = y[6:18]
popt, pcov = curve_fit(ex, x_exp, y_exp)
#print(popt)  # exp coefficient
x_exp_cont = np.arange(x_exp[0], x_exp[-1], 0.001)
plt.loglog(x_exp_cont, ex(x_exp_cont, popt[0], popt[1]), color=s.fit2)
plt.annotate('$I\\sim e^{\\alpha U}$', xy=(4, 6e-11), xytext=(4, 6e-11), color=s.labelcol)

# # --- 19 to end is V^n
def fit(x, y):
    logx = np.log(x)
    logy = np.log(y)
    coeffs = np.polyfit(logx, logy, deg=1)
    poly = np.poly1d(coeffs)
    return poly
x_pow = x[19:]
y_pow = y[19:]
poly1 = fit(x_pow, y_pow)
#print(poly1)  # power coefficient
x_pow_cont = np.arange(x_pow[0], x_pow[-1], 0.001)
yfit1 = lambda x: np.exp(poly1(np.log(x_pow_cont)))
plt.loglog(x_pow_cont, yfit1(x_pow_cont), color=s.fit2)
plt.annotate('$I\\sim U^n$', xy=(8.7, 6e-9), xytext=(8.7, 6e-9), color=s.labelcol)

# --- cosmetics
#plt.title('n-ZnO NR / p-GaN')
plt.xlabel(s.label('U'))
plt.ylabel(s.label('I'))
plt.legend(loc='upper left')
#plt.xlim([0.2, 25])
#plt.ylim([5e-13, 1e-7])

plt.tight_layout()
#plt.savefig('C:\Syncs\Main\Responsible\phd\epfl_roke\pre2\plt\pn01.png')

# --- 3e-12 noise + annotation
#plt.axhline(y=3e-12, color='r', linestyle='--')
#plt.annotate('noise level', xy=(7, 2e-12), xytext=(10, 1.7e-12), color='red')

plt.tight_layout()
#plt.savefig('C:\Syncs\Main\Responsible\phd\epfl_roke\pre2\plt\pn02.png')



# --- d527
d = np.loadtxt(dn[2])
half = int(len(d[:,0])/2)
x,y = -d[:,0][:half], -d[:,1][:half]

# hiding PF part
x_pl = x#[:-18]
y_pl = y#[:-18]

plt.loglog(x_pl, y_pl, 'o', label=s.label_nofib, color=s.data1)

# --- 0 to 10 is exp()
def ex(x, a, b, c):
    return a*np.exp(b*x)+c
def expfit(x, y):
    lx, ly = np.log(x), np.log(y)
    popt, pcov = curve_fit(ex, lx, ly)
    lx_dense = np.arange(lx[0], lx[-1], 0.001)
    fit_vals = ex(lx_dense, *popt)
    return lx_dense, fit_vals
lx, fit_vals = expfit(x[2:10], y[2:10])
plt.loglog(np.exp(lx), np.exp(fit_vals), color=s.fit1)
# plt.loglog(x_exp_cont, ex(x_exp_cont, popt[0], popt[1]), color="#1e76b3")
plt.annotate('$I\\sim e^{\\alpha U}$', xy=(4, 6e-11), xytext=(0.5, 2e-11), color=s.labelcol)

# --- 10 to end is V^n
def fit(x, y):
    logx = np.log(x)
    logy = np.log(y)
    coeffs = np.polyfit(logx, logy, deg=1)
    poly = np.poly1d(coeffs)
    return poly

x_pow = x[10:26]
y_pow = y[10:26]
poly1 = fit(x_pow, y_pow)
print(poly1)  # power coefficient
x_pow_cont = np.arange(x_pow[0], x_pow[-1], 0.001)
yfit1 = lambda x: np.exp(poly1(np.log(x_pow_cont)))
plt.loglog(x_pow_cont, yfit1(x_pow_cont), color=s.fit1)
plt.annotate('$I\\sim U^n$', xy=(2.5, 8e-8), xytext=(1.7, 5e-8), color=s.labelcol)

x_pow = x[30:]
y_pow = y[30:]
poly1 = fit(x_pow, y_pow)
print(poly1)  # power coefficient
x_pow_cont = np.arange(x_pow[0], x_pow[-1], 0.001)
yfit1 = lambda x: np.exp(poly1(np.log(x_pow_cont)))
plt.loglog(x_pow_cont, yfit1(x_pow_cont), color=s.fit1)
plt.annotate('$I\\sim U^2$', xy=(2.5, 8e-8), xytext=(5.5, 1.2e-6), color=s.labelcol)

#plt.annotate('PF', xy=(2.5, 8e-8), xytext=(7.7, 1.2e-6), color='#1e76b3')

ax = plt.gca()
ax.annotate("", xy=(1.8, 3.9e-10), xytext=(1.35, 1.02e-9), arrowprops=dict(arrowstyle="-", linestyle="--", color=s.labelcol2))
ax.annotate("", xy=(5.05, 4.5e-8), xytext=(4.2, 2.3e-7), arrowprops=dict(arrowstyle="-", linestyle="--", color=s.labelcol2))
#ax.annotate("", xy=(7.6, 1.4e-7), xytext=(6.9, 8e-7), arrowprops=dict(arrowstyle="-", linestyle="--"))

ax.annotate("", xy=(10.1, 1.1e-10), xytext=(7.8, 4.3e-10), arrowprops=dict(arrowstyle="-", linestyle="--", color=s.labelcol2))

plt.ylim([1e-13, 5e-6])

plt.legend(loc='upper left')


# ---
plt.tight_layout()

plt.savefig('out/nm_fits.pdf')
plt.savefig('out/nm_fits.pgf')
s.fix_pgf_lyx('out/nm_fits.pgf')
#plt.show()

import os
import glob

import numpy as np

import matplotlib as mpl
mpl.use("pgf")
pgf_with_rc_fonts = {
    "font.family": "serif",
    "font.serif": [],                   # use latex default serif font
    "font.sans-serif": ["DejaVu Sans"], # use a specific sans-serif font
}
mpl.rcParams.update(pgf_with_rc_fonts)

import matplotlib.pyplot as plt

def get_datafiles(directory):
    os.chdir(directory)
    files = []
    for file in glob.glob("*.txt"):
        files.append(file)
    return files

font = {'family': 'sans',
        'weight': 'normal',
        'size': 7,
        }

folder = 'data/fzu/'
files = get_datafiles(folder)

fig = plt.figure(num=None, figsize=(6, 3.5), dpi=100, facecolor='w', edgecolor='k')
labels=["trace", "retrace"]
yellow = "#ffa500"
blue = "#1e76b3"
colors = [blue, yellow]
i=0
for f in files[0:2][::-1]:
    data = np.loadtxt(f, skiprows=1)
    plt.plot(data[:,0], data[:,1], label=labels[i], color=colors[i])
    i+=1
    #np.savetxt('../zpracovani/'+'FZU_'+f.split(" ")[-1].replace('.txt', '')+'.txt', np.c_[data[:,0]*-1, data[:,1]*-1])
    plt.ylabel('$I$ (pA)')
    plt.xlabel('$V$ (V)')
    #plt.text(-10, 160, '* původní data byla s opačnou polaritou', fontdict=font)
    #plt.title('Data z FZU - ZnO tyčka')
    plt.xlim([-10.4, 10.4])
    plt.legend()


#plt.show()
# plt.savefig('../../out/fzu.png')
plt.tight_layout()
plt.savefig('../../out/fzu.pdf')
plt.savefig('../../out/fzu.pgf')

import numpy as np
import matplotlib as mpl
mpl.use("pgf")
pgf_with_rc_fonts = {
    "font.family": "serif",
    "font.serif": [],                   # use latex default serif font
    "font.sans-serif": ["DejaVu Sans"], # use a specific sans-serif font
}
mpl.rcParams.update(pgf_with_rc_fonts)
import matplotlib.pyplot as plt

font = {'family': 'sans',
        'weight': 'normal',
        'size': 10,
        }

def prep_log(data):
    half = int(len(data)/2)
    data_x = data[:,0][0:half]*-1
    data_y = data[:,1][0:half]*-1
    # Filtering values <0 for both x and y
    x = data_x[(data_x>0)&(data_y>0)]
    y = data_y[(data_x>0)&(data_y>0)]
    return x, y

data_top = np.loadtxt('data/nm/ppt_556_low_top.txt')
x_top, y_top = prep_log(data_top)

data_side = np.loadtxt('data/nm/ppt_556_low_side.txt')
x_side, y_side = prep_log(data_side)

def fit(x, y):
    logx = np.log(x)
    logy = np.log(y)
    coeffs = np.polyfit(logx, logy, deg=1)
    poly = np.poly1d(coeffs)
    return poly


# fitting top facet data
poly_top = fit(x_top, y_top)
print(poly_top)
yfit_top = lambda x: np.exp(poly_top(np.log(x_top)))

# fitting side facet data
poly_side = fit(x_side, y_side)
print(poly_side)
yfit_side = lambda x: np.exp(poly_side(np.log(x_side)))

fig = plt.figure(num=1, figsize=(6, 3.5), dpi=100, facecolor='w', edgecolor='k')
plt.loglog(x_top, y_top*1e12, 'o', color="#ffa500", label="top facet")
plt.loglog(x_side, y_side*1e12, 'o', color="#1e76b3", label="side facet")
plt.plot(x_top, yfit_top(x_top)*1e12, color="#ffa500")
plt.text(0.9, 106, '$I\\approx V$', fontdict=font)
plt.plot(x_side, yfit_side(x_side)*1e12, color="#1e76b3")
plt.text(1.7, 34, '$I\\approx V^2$', fontdict=font)
plt.ylabel('$I$ (pA)')
plt.xlabel('$V$ (V)')
plt.legend(loc='best')
#plt.show()
plt.tight_layout()
plt.savefig('out/nm_2' + '.pdf', dpi=300)
plt.savefig('out/nm_2' + '.pgf', dpi=300)
